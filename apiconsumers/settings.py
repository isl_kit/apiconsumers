#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from dotenv import load_dotenv
from environs import Env

env = Env()
load_dotenv('.env')

# COMMON

CWD = env('AC_CWD', '/tmp/api_consumers')
API_URL = env('AC_API_URL', 'http://lecture-translator.kit.edu')
EVENT_PATH = env('AC_EVENT_PATH', 'events/')
RESOURCE_PATH = env('AC_RESOURCES_PATH', 'resources/')
MEDIA_EVENT_PATH = env('AC_MEDIA_EVENT_PATH', 'session-media/events/')

# START / STOP WORKER

ACW_PID_DIR = env('ACW_PID_DIR', 'acw_pid')
ACW_SERVERS = env.list('ACW_SERVERS', ['i13srv30.ira.uka.de', 'i13srv53.ira.uka.de'])
ACW_INSTANCES = env.int('ACW_INSTANCES', 2)
ACW_OFFSET_PRE = env.int('ACW_OFFSET_PRE', 15)
ACW_OFFSET_POST = env.int('ACW_OFFSET_POST', 15)
ACW_COMMAND = env('ACW_COMMAND', 'janus_dbn_online')
ACW_STARTSCRIPTS = env.json('ACW_STARTSCRIPTS', '{"ASR": "start.worker.ASR.adapt","SEG": "start.worker.SEG.adapt"}')
ACW_SUPPORTED_REFINEMENTS = env.list('ACW_SUPPORTED_REFINEMENTS', [
    'ASR',
    'GBI',
    'GMRT',
    'KAWS',
    'MCID',
    'ML1',
    'Programm',
    'RW1',
    'UIT',
    'HMIII_MASCH',
])

# START / STOP ADAPT WORKER

ACA_PID_DIR = env('ACA_PID_DIR', 'aca_pid')
ACA_CONF_DIR = env('ACA_CONF_DIR', 'aca_conf')
ACA_DATA_DIR = env('ACA_DATA_DIR', 'aca_data')
ACA_SERVERS = env.list('ACA_SERVERS', ['i13srv30.ira.uka.de', 'i13srv53.ira.uka.de'])
ACA_INSTANCES = env.int('ACA_INSTANCES', 2)
ACA_OFFSET_PRE = env.int('ACA_OFFSET_PRE', 15)
ACA_OFFSET_POST = env.int('ACA_OFFSET_POST', 15)
ACA_COMMAND = env('ACA_COMMAND', 'janus_dbn_online')
ACA_STARTSCRIPTS = env.json('ACA_STARTSCRIPTS', '{"de": "/project/student_projects/jspeitelsbach/asr/adapt-de-hybrid-v3-so/start.worker.ASR.adaptDict"}')

# START / STOP LECTURE

ACL_PID_DIR = env('ACL_PID_DIR', 'acl_pid')
ACL_SERVERS = env.list('ACL_SERVERS', ['i13srv30.ira.uka.de', 'i13srv53.ira.uka.de'])
ACL_LECTURE_HALL = env.int('ACL_LECTURE_HALL', 1)
ACL_AUDIO_DEVICE = env.int('ACL_AUDIO_DEVICE', 2)
ACL_OFFSET_PRE = env.int('ACL_OFFSET_PRE', 5)
ACL_OFFSET_POST = env.int('ACL_OFFSET_POST', 5)
ACL_COMMAND = env('ACL_COMMAND', 'recording_clien')
ACL_TRANSLATIONS = env.json('ACL_TRANSLATIONS', '{"de": "en", "en": "es"}')
ACL_STARTSCRIPT = env('ACL_STARTSCRIPT', 'recording_client_live.py')

# START / STOP MEDIAEVENTS

ACM_PID_DIR = env('ACM_PID_DIR', 'acm_pid')
ACM_SERVERS = env.list('ACM_SERVERS', ['i13srv30.ira.uka.de', 'i13srv53.ira.uka.de'])
ACM_OFFSET_PRE = env.int('ACM_OFFSET_PRE', 5)
ACM_OFFSET_POST = env.int('ACM_OFFSET_POST', 5)
ACM_TRANSLATIONS = env.json('ACM_TRANSLATIONS', '{"de": "en", "en": "es"}')
ACM_STARTSCRIPT = env('ACM_STARTSCRIPT', 'recording_client_file.py')
