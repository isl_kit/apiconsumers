#!/usr/bin/env python
# -*- coding: utf-8 -*-

import psutil
import requests
import json
import re
import itertools
import subprocess as sb
from os import remove, listdir, makedirs
from os.path import join, exists
from datetime import  datetime, timedelta

from apiconsumers import settings


requests.packages.urllib3.disable_warnings()


def get_current_events():
    url = '{}/{}?offset_pre={}&offset_post={}'.format(
        settings.API_URL,
        settings.EVENT_PATH,
        settings.ACA_OFFSET_PRE,
        settings.ACA_OFFSET_POST
    )
    r = requests.get(url, verify=False)
    if r.status_code != 200:
        print("error requesting current events, request result:", r)
        return None
    events = r.json()
    for event in events:
        slug = event['lecture_term']['lecture']['slug']
        event['lterm'] = event['lecture_term']['id']
        event['slug'] = slug
        event['time_label'] = re.sub('[^0-9]', '', event['start'])
    return events


def get_current_media_events():
    url = '{}/{}?session__isnull=true&offset_pre={}&offset_post={}'.format(
        settings.API_URL,
        settings.MEDIA_EVENT_PATH,
        settings.ACA_OFFSET_PRE,
        settings.ACA_OFFSET_POST
    )
    r = requests.get(url, verify=False)
    if r.status_code != 200:
        return []
    events = r.json()
    result = []
    for event in events:
        result.append({
            'id': event['event']['id'],
            'lterm': event['event']['lecture_term']['id'],
            'slug': event['event']['lecture_term']['lecture']['slug'],
            'time_label': re.sub('[^0-9]', '', event['start']),
            'fingerprint': event['event']['fingerprint']
        })
    return result


def get_event_docs(lterm, event_id):
    url = '{}/{}?lterm={}'.format(
        settings.API_URL,
        settings.RESOURCE_PATH,
        lterm
    )
    r = requests.get(url, verify=False)
    resources = json.loads(r.text)
    url = '{}/{}?event={}&id_only'.format(
        settings.API_URL,
        settings.RESOURCE_PATH,
        event_id
    )
    r = requests.get(url, verify=False)
    event_hashes = [resource['hash'] for resource in json.loads(r.text)]
    files = {}
    for resource in resources:
        files[resource['hash']] = write_data_file(resource['hash'], resource['text'])
    return {
        'files': [f for h, f in files.iteritems()],
        'eventfiles': [f for h, f in files.iteritems() if h in event_hashes]
    }


def write_data_file(name, text):
    data_file = join(settings.CWD, settings.ACA_DATA_DIR, name)
    with open(join(data_file), 'w+') as f:
        f.write(text.encode('utf-8'))
    return data_file


def get_required_workers():
    events = get_current_events() + get_current_media_events()
    workers = []
    for event, server, instance in itertools.product(events, settings.ACA_SERVERS, range(settings.ACA_INSTANCES)):
        worker_id = '{}_{}_{}_{}'.format(event['slug'], event['time_label'], server, instance)
        group_id = '{}_{}'.format(event['slug'], event['time_label'])
        pid_file = join(settings.CWD, settings.ACA_PID_DIR, '{}.pid'.format(worker_id))
        language_tag = event['fingerprint'].split('-')[0]
        if language_tag not in settings.ACA_STARTSCRIPTS:
            continue
        workers.append({
            'event_id': event['id'],
            'lterm': event['lterm'],
            'worker_id': worker_id,
            'group_id': group_id,
            'pid_file': pid_file,
            'server': server,
            'slug': event['slug'],
            'fingerprint': event['fingerprint'],
            'language_tag': language_tag,
            'instance': instance
        })
    return workers


def write_conf_file(worker_id, data):
    conf_file = join(settings.CWD, settings.ACA_CONF_DIR, worker_id + '.json')
    with open(conf_file, 'w+') as f:
        json.dump(data, f, sort_keys=True, indent=4)
        f.write('\n')
    return conf_file


def get_running_pid_files():
    pid_dir = join(settings.CWD, settings.ACA_PID_DIR)
    return [join(pid_dir, f) for f in listdir(pid_dir) if f.endswith('.pid')]


def sanity_check_proc(proc):
    try:
        if proc.name().startswith(settings.ACA_COMMAND):
          return True
        else:
          print("WARNING: sanity check failed! proc.name():", proc.name(), ", settings.ACA_COMMAND:", settings.ACA_COMMAND)
          return False
    except psutil.NoSuchProcess:
        return False


def get_process(pid):
    try:
        proc = psutil.Process(pid)
        return proc if sanity_check_proc(proc) else None
    except psutil.NoSuchProcess:
        return None


def get_pid_from_file(pid_file):
    if not exists(pid_file):
        return None
    with open(pid_file, 'r') as f:
        try:
            pid = int(f.read().strip())
        except ValueError:
            return None
    return pid


def stop_process(pid_file):
    pid = get_pid_from_file(pid_file)
    if pid:
        proc = get_process(pid)
        if proc:
            children = proc.children(recursive=True)
            for p in children + [proc]:
                try:
                    p.kill()
                except psutil.NoSuchProcess:
                    pass
    remove(pid_file)


def stop_unneeded_workers(required_workers):
    required_pid_files = [worker['pid_file'] for worker in required_workers]
    running_pid_files = get_running_pid_files()
    for pid_file in running_pid_files:
        if pid_file not in required_pid_files:
            print("stopping unneeded worker:", pid_file)
            stop_process(pid_file)


def is_running(pid_file):
    pid = get_pid_from_file(pid_file)
    return pid and psutil.pid_exists(pid)


def start_or_keep_running(worker):
    pid_file = worker['pid_file']
    if exists(pid_file):
        if is_running(pid_file):
            return
        remove(pid_file)

    data_files = get_event_docs(worker['lterm'], worker['event_id'])
    config_file = write_conf_file(worker['worker_id'], {
        'lectureSeriesID': worker['slug'],
        'lectureSeriesFiles': data_files['files'],
        'lectureEventFiles': data_files['eventfiles'],
        'fingerprint': "{}-{}".format(worker['fingerprint'], worker['slug']),
        'lectureEventID': worker['group_id'],
        'server': {'hostname': worker['server'], 'port': 60019},
        'instance': worker['instance'],
        'pid': pid_file
    })
    cmd = [settings.ACA_STARTSCRIPTS[worker['language_tag']], config_file]
    print("launching worker", cmd)
    sb.Popen(cmd, cwd=settings.CWD)


def main():
    required_workers = get_required_workers()
    print("found required workers:", required_workers)
    stop_unneeded_workers(required_workers)
    for worker in required_workers:
        start_or_keep_running(worker)


if __name__ == '__main__':
    main()
