#!/home/bkrueger/.virtualenvs/apic/bin/python
# -*- coding: utf-8 -*-

import psutil
import requests
import json
import re
import itertools
import subprocess as sb
from os import walk, remove, listdir, setsid, getpgid, makedirs
from os.path import join, exists, splitext, basename
from datetime import  datetime, timedelta

from apiconsumers import settings


requests.packages.urllib3.disable_warnings()


def get_current_events():
    r = requests.get('{}/{}?session__isnull=true&offset_pre={}&offset_post={}'.format(
        settings.API_URL,
        settings.MEDIA_EVENT_PATH,
        settings.ACM_OFFSET_PRE,
        settings.ACM_OFFSET_POST
    ), verify=False)
    if r.status_code != 200:
        return []
    events = r.json()
    for event in events:
        ident = 'ME_{}'.format(event['id'])
        event['ident'] = ident
        event['tag'] = event['fingerprint'].split('-')[0]
        event['translation'] = settings.ACM_TRANSLATIONS.get(event['tag'], '')
    return events


def get_running_pid_files():
    pid_dir = join(settings.CWD, settings.ACM_PID_DIR)
    return [join(pid_dir, f) for f in listdir(pid_dir) if splitext(f)[1] == '.pid']


def get_log_file(ident):
    return join(settings.CWD, ident, 'events.log')


def get_main_log_file(ident):
    return join(settings.CWD, ident, 'main.log')


def get_pid_file(ident):
    return join(settings.CWD, settings.ACM_PID_DIR, ident + '.pid')


def get_param_file(ident, name):
    return join(settings.CWD, ident, name)


def get_pid(pid_file):
    if not exists(pid_file):
        return None
    with open(pid_file, 'r') as f:
        try:
            return int(f.read().strip())
        except ValueError:
            return None


def get_param(ident, name, default=0, ftype=int):
    file_name = get_param_file(ident, name)
    if not exists(file_name):
        return default
    with open(file_name, 'r') as f:
        try:
            return ftype(f.read().strip())
        except ValueError:
            return default


def set_param(ident, name, value):
    file_name = get_param_file(ident, name)
    with open(file_name, 'w+') as f:
        f.write(str(value) + '\n')


def log(ident, msg):
    log_file = get_log_file(ident)
    prefix = datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ' | '
    empty_prefix = ''.join([' ' for _ in prefix])
    with open(log_file, 'a+') as f:
        msg = msg.replace('\n', '\n' + empty_prefix)
        f.write(prefix + msg + '\n')


def create_event_dir(ident):
    event_dir = join(settings.CWD, ident)
    if not exists(event_dir):
        makedirs(event_dir)


def clean_up(ident):
    pid_file = get_pid_file(ident)
    if exists(pid_file):
        remove(pid_file)


def get_next_server_idx(ident):
    si = get_param(ident, 'si')
    rc = get_param(ident, 'rc')
    if rc in [3, 4, 5]:
        return si + 1 if si + 1 < len(settings.ACM_SERVERS) else 0
    else:
        return si


def get_ident_from_pid_file(pid_file):
    file_name = basename(pid_file)
    ident, _ = splitext(file_name)
    return ident


def restart_or_remove_running_events():
    running_pid_files = get_running_pid_files()
    for pid_file in running_pid_files:
        pid = get_pid(pid_file)
        if not is_running(pid):
            ident = get_ident_from_pid_file(pid_file)
            return_code = get_param(ident, 'rc')
            log(ident, 'Session Stopped : RC: {}'.format(return_code))
            clean_up(ident)


def is_running(pid):
    return pid and psutil.pid_exists(pid)


def start_event_if_not_running(event):
    ident = event['ident']
    pid_file = get_pid_file(ident)
    if exists(pid_file):
        if is_running(pid_file) or get_param(ident, 'rc') == 0:
            return
        remove(pid_file)
    create_event_dir(ident)
    si = get_next_server_idx(ident)
    title = u'"Scheduled Event {id}?N=L?INV'.format(**event)
    if event['translation']:
        title += u'?T={translation}?SUB={translation}'.format(**event)
    title += u'?ME={id}"'.format(**event)
    cmd = [
        settings.ACM_STARTSCRIPT,
        '-S', settings.ACM_SERVERS[si],
        '-t', title,
        '-d', '"Scheduled event: ME_{id}"'.format(**event),
        '-fi', '{fingerprint}'.format(**event),
        '-fo', '{tag}'.format(**event),
        '-w', 'secure',
        '--store',
        '-po',
        '-f', event['audio_path'],
        '>>{}'.format(get_main_log_file(ident)),
        '2>/dev/null',
        '; echo $? > {}'.format(get_param_file(ident, 'rc'))
    ]
    cmd = u' '.join(cmd)
    set_param(ident, 'si', si)
    set_param(ident, 'cmd', cmd.encode('utf-8'))
    pid = run_command(cmd, pid_file)
    log(ident, 'Session Starting: PID: {}, SI: {}'.format(pid, si))


def run_command(cmd, pid_file):
    pid = getpgid(sb.Popen(cmd, cwd=settings.CWD, shell=True, preexec_fn=setsid).pid)
    with open(pid_file, 'w+') as f:
        f.write(str(pid) + '\n')
    return pid


def main():
    current_events = get_current_events()
    for event in current_events:
        start_event_if_not_running(event)


if __name__ == '__main__':
    main()
