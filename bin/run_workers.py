#!/usr/bin/env python
# -*- coding: utf-8 -*-

import psutil
import requests
import json
import re
import itertools
import subprocess as sb
from os import remove, listdir
from os.path import join, exists
from datetime import  datetime, timedelta

from apiconsumers import settings


requests.packages.urllib3.disable_warnings()


def get_current_events():
    url = '{}/{}?offset_pre={}&offset_post={}'.format(
        settings.API_URL,
        settings.EVENT_PATH,
        settings.ACW_OFFSET_PRE,
        settings.ACW_OFFSET_POST
    )
    r = requests.get(url, verify=False)
    if r.status_code != 200:
        return []
    events = r.json()
    for event in events:
        slug = event['lecture_term']['lecture']['slug']
        event['slug'] = slug
        event['time_label'] = re.sub('[^0-9]', '', event['start'])
    events = [event for event in events if event['slug'] in settings.ACW_SUPPORTED_REFINEMENTS]
    return events


def get_required_workers():
    events = get_current_events()
    workers = []
    for event, server, instance, (wtype, executable) in itertools.product(events, settings.ACW_SERVERS, range(settings.ACW_INSTANCES), settings.ACW_STARTSCRIPTS.iteritems()):

        pid_file = join(settings.CWD, settings.ACW_PID_DIR, '{}_{}_{}_{}_{}.pid'.format(event['slug'], event['time_label'], server, instance, wtype))
        workers.append({
            'pid_file': pid_file,
            'executable': executable,
            'server': server,
            'slug': event['slug'],
            'instance': instance
        })
    return workers


def get_running_pid_files():
    pid_dir = join(settings.CWD, settings.ACW_PID_DIR)
    return [join(pid_dir, f) for f in listdir(pid_dir) if f.endswith('.pid')]


def sanity_check_proc(proc):
    try:
        return proc.name() == settings.ACW_COMMAND
    except psutil.NoSuchProcess:
        return False


def get_process(pid):
    try:
        proc = psutil.Process(pid)
        return proc if sanity_check_proc(proc) else None
    except psutil.NoSuchProcess:
        return None


def get_pid_from_file(pid_file):
    if not exists(pid_file):
        return None
    with open(pid_file, 'r') as f:
        try:
            pid = int(f.read().strip())
        except ValueError:
            return None
    return pid


def stop_process(pid_file):
    pid = get_pid_from_file(pid_file)
    if pid:
        proc = get_process(pid)
        if proc:
            try:
                proc.kill()
            except psutil.NoSuchProcess:
                pass
    remove(pid_file)


def stop_unneeded_workers(required_workers):
    required_pid_files = [worker['pid_file'] for worker in required_workers]
    running_pid_files = get_running_pid_files()
    for pid_file in running_pid_files:
        if pid_file not in required_pid_files:
            stop_process(pid_file)


def is_running(pid_file):
    pid = get_pid_from_file(pid_file)
    return pid and psutil.pid_exists(pid)


def start_or_keep_running(worker):
    pid_file = worker['pid_file']
    if exists(pid_file):
        if is_running(pid_file):
            return
        remove(pid_file)
    cmd = [worker['executable'], worker['slug'], worker['server'], str(worker['instance']), worker['pid_file']]
    sb.Popen(cmd, cwd=CWD)


def main():
    required_workers = get_required_workers()
    stop_unneeded_workers(required_workers)
    for worker in required_workers:
        start_or_keep_running(worker)


if __name__ == '__main__':
    main()
