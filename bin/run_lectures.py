#!/usr/bin/env python
# -*- coding: utf-8 -*-

import psutil
import requests
import traceback
import json
import re
import itertools
import subprocess as sb
from signal import SIGKILL
from os import remove, listdir, setsid, killpg, kill, getpgid, makedirs
from os.path import join, exists, splitext, basename
from datetime import  datetime, timedelta

from apiconsumers import settings


requests.packages.urllib3.disable_warnings()


def get_current_events():
    url = '{}/{}?hall={}&offset_pre={}&offset_post={}'.format(
        settings.API_URL,
        settings.EVENT_PATH,
        settings.ACL_LECTURE_HALL,
        settings.ACL_OFFSET_PRE,
        settings.ACL_OFFSET_POST
    )
    r = requests.get(url, verify=False)
    if r.status_code != 200:
        return []
    events = r.json()
    for event in events:
        slug = event['lecture_term']['lecture']['slug']
        start = datetime.strptime(event['start'], '%Y-%m-%dT%H:%M:%S')
        time_label = start.strftime('%y%m%d%H%M')
        event['slug'] = slug
        event['title'] = event['lecture_term']['lecture']['title']
        event['tag'] = event['fingerprint'].split('-')[0]
        event['translation'] = settings.ACL_TRANSLATIONS.get(event['tag'], '')
        event['start_string'] = start.strftime('%Y-%m-%d %H:%M')
        event['ident'] = '{}_{}_{}'.format(settings.ACL_LECTURE_HALL, slug, time_label)
    return events


def get_param_file(ident, name):
    return join(settings.CWD, ident, '.' + name)


def get_param(ident, name, default=0, ftype=int):
    file_name = get_param_file(ident, name)
    if not exists(file_name):
        return default
    with open(file_name, 'r') as f:
        try:
            return ftype(f.read().strip())
        except ValueError:
            return default


def set_param(ident, name, value):
    file_name = get_param_file(ident, name)
    with open(file_name, 'w+') as f:
        f.write(str(value) + '\n')


def create_event_dir(ident):
    event_dir = join(settings.CWD, ident)
    if not exists(event_dir):
        makedirs(event_dir)


def get_next_server_idx(ident):
    si = get_param(ident, 'si')
    rc = get_param(ident, 'rc')
    if rc in [3, 4, 5]:
        return si + 1 if si + 1 < len(settings.ACL_SERVERS) else 0
    else:
        return si


def get_pid_file(ident):
    return join(settings.CWD, settings.ACL_PID_DIR, ident + '.pid')


def get_log_file(ident):
    return join(settings.CWD, ident, 'events.log')


def get_main_log_file(ident):
    return join(settings.CWD, ident, 'main.log')


def log(ident, msg):
    log_file = get_log_file(ident)
    prefix = datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ' | '
    empty_prefix = ''.join([' ' for _ in prefix])
    with open(log_file, 'a+') as f:
        msg = msg.replace('\n', '\n' + empty_prefix)
        f.write(prefix + msg + '\n')


def get_ident_from_pid_file(pid_file):
    file_name = basename(pid_file)
    ident, _ = splitext(file_name)
    return ident


def get_running_pid_files():
    pid_dir = join(settings.CWD, settings.ACL_PID_DIR)
    return [join(pid_dir, f) for f in listdir(pid_dir) if splitext(f)[1] == '.pid']


def sanity_check_proc(proc):
    try:
        return proc.name() == settings.ACL_COMMAND
    except psutil.NoSuchProcess:
        return False


def get_process(pid):
    try:
        proc = psutil.Process(pid)
        ch = proc.children(recursive=True)
        if len(ch) == 0:
            return None
        return ch[0] if sanity_check_proc(ch[0]) else None
    except psutil.NoSuchProcess:
        return None


def get_pid_from_file(pid_file):
    if not exists(pid_file):
        return None
    with open(pid_file, 'r') as f:
        try:
            pid = int(f.read().strip())
        except ValueError:
            return None
    return pid


def stop_process(pid_file):
    pid = get_pid_from_file(pid_file)
    if pid:
        proc = get_process(pid)
        if proc:
            try:
                kill(proc.pid, SIGKILL)
            except:
                pass
    remove(pid_file)


def stop_unneeded_clients(current_events):
    required_pid_files = [get_pid_file(event['ident']) for event in current_events]
    running_pid_files = get_running_pid_files()
    for pid_file in running_pid_files:
        if pid_file not in required_pid_files:
            ident = get_ident_from_pid_file(pid_file)
            log(ident, 'Session Stopped')
            stop_process(pid_file)


def is_running(pid_file):
    pid = get_pid_from_file(pid_file)
    return pid and psutil.pid_exists(pid)


def start_or_keep_running(event):
    ident = event['ident']
    pid_file = get_pid_file(ident)
    if exists(pid_file):
        if is_running(pid_file):
            return
        remove(pid_file)
    create_event_dir(ident)
    si = get_next_server_idx(ident)
    title = u'"{title}?N=L'.format(**event)
    if event['translation']:
        title += u'?T={translation}?SUB={translation}'.format(**event)
    title += u'?S={slug}?D={start_string}?C={ident}"'.format(**event)
    cmd = [
        settings.ACL_STARTSCRIPT,
        '-S', settings.ACL_SERVERS[si],
        '-t', title,
        '-d', '"Scheduled event: {slug}"'.format(**event),
        '-fi', '{fingerprint}-{slug}'.format(**event),
        '-fo', '{tag}'.format(**event),
        '-w', 'secure',
        '--store',
        '-a', str(settings.ACL_AUDIO_DEVICE),
        '>>{}'.format(get_main_log_file(ident)),
        '2>/dev/null',
        '; echo $? > {}'.format(get_param_file(ident, 'rc'))
    ]
    cmd = u' '.join(cmd)
    set_param(ident, 'si', si)
    set_param(ident, 'cmd', cmd.encode('utf-8'))
    pid = run_command(cmd, pid_file)
    log(ident, 'Session Starting: PID: {}, SI: {}'.format(pid, si))


def run_command(cmd, pid_file):
    pid = sb.Popen(cmd, cwd=settings.CWD, shell=True).pid
    with open(pid_file, 'w+') as f:
        f.write(str(pid) + '\n')
    return pid


def main():
    current_events = get_current_events()
    stop_unneeded_clients(current_events)
    for event in current_events:
        start_or_keep_running(event)


if __name__ == '__main__':
    main()
