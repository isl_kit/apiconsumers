# API Consumers

This repository is a collection of several Python scripts, that consume the Lecture Translator API and start different kind of processes.

## Requirements

    pip install git+ssh://git@bitbucket.org/isl_kit/pythonrecordingclient.git

## Install

    pip install git+ssh://git@bitbucket.org/isl_kit/apiconsumers.git

## Config

Configuration is done with environment variables. Those variables can also be defined in a `.env` file, located in the directory from which the individual scripts are called.

The following shows a list of all the settings, that are shared among the individual scripts. Settings that are specific to one script, are listed in the section of that script.

#### AC_CWD

The working directory.

    AC_CWD=/tmp/api_consumers

#### AC_API_URL

The URL to the Lecture Translator API.

    AC_API_URL=http://lecture-translator.kit.edu

#### AC_EVENT_PATH

The path, relative to `API_URL`, to access the Event API.

    AC_EVENT_PATH=events/

#### AC_RESOURCE_PATH

The path, relative to `API_URL`, to access the Resource API.

    AC_RESOURCE_PATH=resources/

#### AC_MEDIA_EVENT_PATH

The path, relative to `API_URL`, to access the MediaEvent API.

    AC_MEDIA_EVENT_PATH=session-media/events/


## run_lectures.py

This script starts a lecture recording, based on the events in the Lecture Translator database. It is supposed to be used, to automatically start recordings in supported lecture halls.

The script sends an API request to the URL

    https://lecture-translator.kit.edu/events/?hall=[HALLID]&offset_pre=5&offset_post=5

This returns all events of the lecture hall with ID `[HALLID]`, that are supposed to run now (with an offset of 5min). The script then starts a `recording_client_live.py` process ([Python RecordingClient](https://bitbucket.org/isl_kit/pythonrecordingclient.git)) to start a session and stream the audio from a specified audio interface.

This script is supposed to be started periodically (i.e. `while true; do run_lectures.py; sleep 60; done`).

### Config

#### ACL_PID_DIR

The directory that should contain the PID files (relative to `CWD`).

    ACL_PID_DIR=acl_pid

#### ACL_SERVERS

The Mediator servers, that should be considered. If, for whatever reason, the session on the first Mediator server fails to start, the second one will be used, and so on.

    ACL_SERVERS=i13srv30.ira.uka.de,i13srv53.ira.uka.de

#### ACL_LECTURE_HALL

The ID of the `LectureHall` object, to which running events

    ACL_LECTURE_HALL=1

#### ACL_AUDIO_DEVICE

The ID of the audio device that should be used for recording.

    ACL_AUDIO_DEVICE=2

To get a list of all audio devices, use:

    recording_client_live.py -L 1

#### ACL_OFFSET_PRE

Together with ACL_OFFSET_POST, this defines a time interval that centers around the current time and reaches `ACL_OFFSET_PRE` minutes into the past and `ACL_OFFSET_POST` minutes into the future. All events are matched that overlap with this interval.

    ACL_OFFSET_PRE=5

#### ACL_OFFSET_POST

    ACL_OFFSET_POST=5

#### ACL_COMMAND

This command name is used for sanity checking, when killing a session that no longer is supposed to run. The process that is about to be killed, must have this name, otherwise it will be left alone.

    ACL_COMMAND=recording_clien

#### ACL_TRANSLATIONS

A JSON object, that defines what default translation should be used for which input language.

    ACL_TRANSLATIONS={"de": "en", "en": "es"}

#### ACL_STARTSCRIPT

The name (or full path) to the recording_client_live.py script.

    ACL_STARTSCRIPT=recording_client_live.py

## run_media_events.py

This script starts a session, based on the media events in the Lecture Translator database.

The script sends an API request to the URL

    https://lecture-translator.kit.edu/session-media/events/?session__isnull=true&offset_pre=5&offset_post=5

This returns all unprocessed media events, that are supposed to run now (with an offset of 5min). The script then starts a `recording_client_file.py` process ([Python RecordingClient](https://bitbucket.org/isl_kit/pythonrecordingclient.git)) to start a session and stream the audio from the source file defined on the `MediaEvent` object.

This script is supposed to be started periodically (i.e. `while true; do run_media_events.py; sleep 60; done`).

### Config

#### ACM_PID_DIR

The directory that should contain the PID files (relative to `CWD`).

    ACM_PID_DIR=acm_pid

#### ACM_SERVERS

The Mediator servers, that should be considered. If, for whatever reason, the session on the first Mediator server fails to start, the second one will be used, and so on.

    ACM_SERVERS=i13srv30.ira.uka.de,i13srv53.ira.uka.de

#### ACM_OFFSET_PRE

Together with ACM_OFFSET_POST, this defines a time interval that centers around the current time and reaches `ACM_OFFSET_PRE` minutes into the past and `ACM_OFFSET_POST` minutes into the future. All events are matched that overlap with this interval.

    ACM_OFFSET_PRE=5

#### ACM_OFFSET_POST

    ACM_OFFSET_POST=5

#### ACM_TRANSLATIONS

A JSON object, that defines what default translation should be used for which input language.

    ACM_TRANSLATIONS={"de": "en", "en": "es"}

#### ACM_STARTSCRIPT

The name (or full path) to the recording_client_file.py script.

    ACM_STARTSCRIPT=recording_client_file.py


## run_adapted_workers.py

This script starts a worker with fast adaptation, based on the events and media events in the Lecture Translator database.

First, the script sends an API request to the URLs

    https://lecture-translator.kit.edu/events/?offset_pre=5&offset_post=5
    https://lecture-translator.kit.edu/session-media/events/?session__isnull=true&offset_pre=5&offset_post=5

This returns all events and unprocessed media events, that are supposed to run now (with an offset of 5min). For each of these events, the script ensures that the appropriate workers are running. If for one event a new worker needs to be started, two requests will be sent to fetch the associated documents:

    https://lecture-translator.kit.edu/resources/?lterm=[LECTURETERMID]
    https://lecture-translator.kit.edu/resources/?event=[EVENTID]&id_only

The first request return all documents associated with the whole `LectureTerm` object, the second one then returns the document IDs of those documents that are only associated with the exact `Event` object.

The fetched text files are then written to disk and for each worker a configuration file is written. The actual worker is then started:

    /path/to/start.worker.ASR.adaptDict /path/to/config.json

This script is supposed to be started periodically (i.e. `while true; do run_adapted_workers.py; sleep 60; done`).

### Config

#### ACA_PID_DIR

The directory that should contain the PID files (relative to `CWD`).

    ACA_PID_DIR=aca_pid

#### ACA_CONF_DIR

The directory where config files should be written (relative to `CWD`).

    ACA_CONF_DIR=aca_conf

#### ACA_DATA_DIR

The directory where data files should be writter (relative to `CWD`).

    ACA_DATA_DIR=aca_data

#### ACA_SERVERS

The Mediator servers, to which the workers should connect. There will be one worker instance for each defined server.

    ACA_SERVERS=i13srv30.ira.uka.de,i13srv53.ira.uka.de

#### ACA_INSTANCES

An integer that defines how may parallel instances of one worker should be started.

    ACA_INSTANCES=2

#### ACA_OFFSET_PRE

Together with ACA_OFFSET_POST, this defines a time interval that centers around the current time and reaches `ACA_OFFSET_PRE` minutes into the past and `ACA_OFFSET_POST` minutes into the future. All events are matched that overlap with this interval.

    ACA_OFFSET_PRE=15

#### ACA_OFFSET_POST

    ACA_OFFSET_POST=15

#### ACA_COMMAND

This command name is used for sanity checking, when killing a session that no longer is supposed to run. The process that is about to be killed, must have this name, otherwise it will be left alone.

    ACA_COMMAND=janus_dbn_online

#### ACA_STARTSCRIPTS

A JSON objects, that defines the worker start scripts for each input language.

    ACA_STARTSCRIPTS={"de":"/project/student_projects/jspeitelsbach/asr/adapt-de-hybrid-v3-so/start.worker.ASR.adaptDict"}

## run_workers.py

This script starts a worker that are pre-adapted to a certain lecture, based on the events and media events in the Lecture Translator database.

This script can probably be considered deprecated and would otherwise be in need of a review.
