#!/usr/bin/python
# -*- coding: utf-8 -*-

from distutils.core import setup

setup(
    name="APIConsumers",
    version="1.0.0",
    author="Bastian Krüger",
    author_email="bastian.krueger@kit.edu",
    packages=["apiconsumers"],
    scripts=["bin/run_adapted_workers.py", "bin/run_lectures.py", "bin/run_media_events.py", "bin/run_workers.py"],
    url="https://bitbucket.org/isl_kit/apiconsumers",
    license="LICENSE.txt",
    description="Several Python tools, that consume the Lecture Translator API and start other processes depending on the results.",
    long_description=open("README.md").read(),
    install_requires=[
        'python-dotenv>=0.6.4',
        'environs>=1.2.0',
        'psutil>=5.2.2',
        'requests>=2.18.1'
    ],
    zip_safe = False,
)
